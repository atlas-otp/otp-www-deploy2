<?php

$since = 21;        // days

$projects = [
    ['atlas-web', 'atlasop', 'opened', '4.1.4'],
    ['atlas-web', 'atlasop', 'closed', null],

    ['atlas-otp', 'AtlasOTP', 'opened', '1.13.10'],
    ['atlas-otp', 'AtlasOTP', 'opened', '1.13.11'],
    ['atlas-otp', 'AtlasOTP', 'opened', 'Tasks'],
    ['atlas-otp', 'AtlasOTP', 'closed', null],

    ['atlas-otp', 'otp-appointment', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-appointment', 'closed', null],

    ['atlas-otp', 'otp-agenda', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-agenda', 'closed', null],

    ['atlas-otp', 'otp-calendar', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-calendar', 'closed', null],

    ['atlas-otp', 'otp-csv', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-csv', 'closed', null],

    ['atlas-otp', 'otp-db-batch', 'opened', '1.13.10'],
    ['atlas-otp', 'otp-db-batch', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-db-batch', 'closed', null],

    ['atlas-otp', 'otp-db-performance', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-db-performance', 'closed', null],

    ['atlas-otp', 'otp-docs', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-docs', 'closed', null],

    ['atlas-otp', 'otp-editor', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-editor', 'closed', null],

    ['atlas-otp', 'otp-mail', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-mail', 'closed', null],

    ['atlas-otp', 'otp-next', 'opened', '0.5'],
    ['atlas-otp', 'otp-next', 'opened', '0.6'],
    ['atlas-otp', 'otp-next', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-next', 'closed', null],

    ['atlas-otp', 'otp-next-cron', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-next-cron', 'closed', null],

    ['atlas-otp', 'otp-photos', 'opened', '1.1'],
    ['atlas-otp', 'otp-photos', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-photos', 'closed', null],

    ['atlas-otp', 'otp-reports', 'opened', '5.3.0'],
    ['atlas-otp', 'otp-reports', 'opened', '5.3.1'],
    ['atlas-otp', 'otp-reports', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-reports', 'closed', null],

    ['atlas-otp', 'otp-qualification', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-qualification', 'closed', null],

    ['atlas-otp', 'otp-tools', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-tools', 'closed', null],

    ['atlas-otp', 'otp-user-manual', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-user-manual', 'closed', null],

    ['atlas-otp', 'otp-www', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-www', 'closed', null],

    ['atlas-otp', 'otp-www-next', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-www-next', 'closed', null],

    ['atlas-otp', 'otp-www-cron', 'opened', 'Tasks'],
    ['atlas-otp', 'otp-www-cron', 'closed', null]
];

$per_page = 50;

// BELOW NO CONFIGURATION
if (file_exists(stream_resolve_include_path('token.php'))) {
    include 'token.php';
} else {
    include 'token_env.php';
}

date_default_timezone_set('Europe/Amsterdam');

function get($url, $token) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER,array("PRIVATE-TOKEN: $token"));
    curl_setopt($ch, CURLOPT_URL,$url);
    $result=curl_exec($ch);
    curl_close($ch);
    return json_decode($result, true);
}

function startswith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

$data = array();

foreach($projects as $project) {

    $group = $project[0];
    $name = $project[1];
    $state = $project[2];
    $milestone = $project[3];

    // get the info
    $url = "https://gitlab.cern.ch/api/v4/projects/$group%2F$name/issues?state=$state&milestone=$milestone&per_page=$per_page";
    $issues = get($url, $token);

    // print_r($issues);

    foreach($issues as $issue) {
        // create output
        $row = array();

        $group_name_project_name_issue_no = $issue['references']['full'];

        list($group_name, $project_name_issue_no) = explode("/", $group_name_project_name_issue_no);
        list($project_name, $issue_no) = explode("#", $project_name_issue_no);

        $row[] = $group_name;
        $row[] = $project_name;
        $row[] = $issue_no;
        $row[] = $issue['state'];
        $row[] = $issue['updated_at'];
        $row[] = $issue['title'];
        // $row[] = $issue['description'];
        $row[] = $issue['web_url'];
        $row[] = $issue['labels'];
        if ($issue['milestone']) {
            $row[] = $issue['milestone']['title'];
        } else {
            $row[] = "";
        }

        $closed_at = $issue['closed_at'];
        if ($closed_at) {
            $closed_at_date = new DateTime($closed_at);
            $now = new DateTime();
            $days  = $now->diff($closed_at_date)->format('%a');
            if ($days < $since) {
                // add the row
                $data[] = $row;
            }
        } else {
            // add the row
            $data[] = $row;
        }
    }
}

// add initial key
$output['data'] = $data;

print(json_encode($output, JSON_PRETTY_PRINT));
?>
