$(document).ready(function() {
    var colGroup = 0;
    var colProject = 1;
    var colIssue = 2
    var colState = 3;
    var colDate = 4;
    var colTitle = 5;
    var colUrl = 6;
    var colLabels = 7;
    var colMilestone = 8;

    var table = $('#issues').DataTable( {
        ajax: 'issues-data.php',
        paging: false,
        searching: false,
        autoWidth: true,
        info: false,
        order: [[ colState, "asc" ], [ colDate, "asc" ], [ colProject, "asc" ], [ colMilestone, "asc" ]],
        columnDefs: [
            {
                targets: [colDate],
                render: function( data, type, row, meta) {
                   return type == 'display' ? data.split('T')[0] : data;
                }
            },
            {
                targets: [colLabels],
                render: function ( data, type, row, meta ) {
                    return data;
                },
            },
            {
                targets: [colTitle],
                render: function ( data, type, row, meta ) {
                    var bold = row[colState] != 'closed';
                    return (bold ? '<b>' : '') + '<a href="' + row[colUrl] + '">' + data + '</a>' + (bold ? '</b>' : '');
                },
            },
            {
                targets: [colUrl],
                visible: false,
            }
        ],
    } );
} );
