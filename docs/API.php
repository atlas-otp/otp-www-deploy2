<!DOCTYPE html>

<!-- warning.html -->

<!--------------------------------------------------------------------------->
<!--                                                                       -->
<!-- WARNING, this file is generated, please edit the source and reconvert -->
<!--                                                                       -->
<!--                                                                       -->
<!--                                                                       -->
<!--------------------------------------------------------------------------->



<?php
error_reporting(E_ALL);
ini_set("display_errors", '1');
?>


<?php
date_default_timezone_set("Europe/Zurich");
$dateformat = "d-m-y H:i:s";
$utcformat = "D, d M Y H:i:s T";
$updatetime = 15*60;

set_include_path(get_include_path() . PATH_SEPARATOR . '../');
?>


<html lang="en">
    <head>
        <!-- header.html -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="OTP, Operation Task Planner">
<meta name="keywords" content="CERN, ATLAS, OTP">
<meta name="author" content="Mark Donszelmann">
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">

<title>
   &middot; OTP ATLAS
</title>

<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../ico/atlas-144x144.png">
                               <link rel="shortcut icon" href="../ico/atlas.ico">

<!-- jQuery -->
<!-- NOTE: no Ajax in 'slim' version of jquery -->
<!-- NOTE: jquery 3.x did not work -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- jQuery TimeAgo -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.6.3/jquery.timeago.min.js"></script>

<!-- jQuery Resizable -->
<!--
<script src="../jquery-resizable-0.28/dist/jquery-resizable.min.js"></script>
-->

<!-- Bootstrap -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.2.1/cerulean/bootstrap.min.css">
<!--
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css">
-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" >

<!-- Bootstrap Select -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/css/bootstrap-select.min.css">

<!-- Bootstrap DateRangePicker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js"></script>

<!-- Datatables, jquery then bootstrap, but NOTE removal below -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.min.css" >
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
<!-- Removing container-fluid on class of dataTables_wrapper as it interferes with scrollResize -->
var DataTable = $.fn.dataTable;
$.extend( DataTable.ext.classes, {
    sWrapper:      "dataTables_wrapper dt-bootstrap4",
} );
</script>

<!-- Datatables Extensions -->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.0.3/css/rowGroup.dataTables.min.css">
<script src="https://cdn.datatables.net/rowgroup/1.0.3/js/dataTables.rowGroup.min.js"></script>

<!-- Does not work with scrollResize
<link rel="stylesheet" href="https://cdn.datatables.net/scroller/1.5.0/css/scroller.dataTables.min.css">
<script src="https://cdn.datatables.net/scroller/1.5.0/js/dataTables.scroller.min.js"></script>
-->

<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.4/css/fixedHeader.dataTables.min.css">
<script src="https://cdn.datatables.net/fixedheader/3.1.4/js/dataTables.fixedHeader.min.js"></script>

<!-- Datatables Plugins -->
<script src="https://cdn.datatables.net/plug-ins/1.10.19/features/scrollResize/dataTables.scrollResize.min.js"></script>

<!-- Highcharts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/highcharts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/modules/exporting.js"></script>

<!-- Search JS -->
<!--
Below are possibly these ?
<script src="https://cdnjs.cloudflare.com/ajax/libs/lunr.js/2.0.1/lunr.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="http://stevenlevithan.com/assets/misc/date.format.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/URI.js/1.18.10/URI.min.js" type="text/javascript"></script>
-->
<!--
<script src="../js/search.min.js" type="text/javascript"></script>
-->
<script src="../js/lunr.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/mustache.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/date.format.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/URI.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/jquery.lunr.search.js" type="text/javascript" charset="utf-8"></script>

<!-- ATLAS and OTP Stylesheets and JavaScript-->
<link rel="stylesheet" href="../css/atlas.css" >
<link rel="stylesheet" href="../css/otp.css" >
<script src="../js/otp.js" type="text/javascript"></script>




        
        
    </head>

    <body>
        <?php
if (str_contains($_SERVER['SERVER_NAME'], '-alpha')) {
  $report_server = "https://otp-reports-atlas-alpha.web.cern.ch";
  $web_server = "https://otp-atlas.web.cern.ch";
  $web_server_text = "Goto Production Website";
} else {
  $report_server = "https://otp-reports-atlas.web.cern.ch";
  $web_server = "https://otp-www-atlas-alpha.web.cern.ch";
  $web_server_text = "Goto Staged Website";
}
?>


<div class="container">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="../">ATLAS OTP</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

        <li class="nav-item">

        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Shifts
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../docs/QuickStart.php">Quick Start</a>
            <a class="dropdown-item" href="../shift-classes/index.php">Shift Classes and Requirements</a>
            <a class="dropdown-item" href="../docs/ControlRoomShifts.php">ATLAS Control Room Shifts</a>
            <a class="dropdown-item" href="https://atlasop.cern.ch/atlas-point1/shifts">Safety Rules for Shifters</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Documentation
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../docs/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../docs/WhatIsOTP.pdf" target="WhatIsOTP">What is OTP</a>
            <a class="dropdown-item" href="https://otp-atlas.docs.cern.ch" target="OTPUserManual">OTP User Manual</a>
            <a class="dropdown-item" href="../docs/API.php">Public API</a>
            <a class="dropdown-item" href="../docs/AUTHDB.php">AUTHDB Usage</a>
            <a class="dropdown-item" href="../docs/Calculations.php">Calculations</a>
            <a class="dropdown-item" href="../docs/FAQ.php">FAQ</a>
            <a class="dropdown-item" href="../docs/OTvalues.php">OT Values</a>
            <a class="dropdown-item" href="../docs/Photos.php">Photos</a>
            <a class="dropdown-item" href="../docs/SignupForShifts.php">How to Signup for Shifts</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Tools
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="https://atlas-otp.cern.ch/" target="otp-tool">OTP Tool...</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Reports
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="<?= $report_server ?>/reports/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/TaskScheduleP1.php">Task Schedule Point1</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/FundingAgency.php">Funding Agency</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/FundingAgency.php?byInstitution">Funding Agency by Institution</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Institution.php">Institution</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/InstitutionYear.php">Institution by Year</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Task.php">Task</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/MultiYear.php">Multi-Year</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/MultiYear.php?byInstitution">Multi-Year by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Commitment.php">Commitment</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/CommitmentFunding.php">Commitment Funding</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/CommitmentFunding.php?byInstitution">Commitment Funding by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/PersonalSchedule.php">Personal Schedule</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/TaskSchedule.php">Task Schedule</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Occupancy.php">Occupancy</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Personal.php">Personal</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/ShiftsByHour.php">Shifts by Hour (SLIMOS)</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/TaskPersonnel.php">Task Personnel</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Allocation.php">Allocation</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Requirement.php">Requirement</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/History.php">History</a>
          </div>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?= $report_server ?>/final_reports/index.php">Final Reports</a>
        </li>

      <!--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Plots
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="<?= $report_server ?>/plots/index.php">Description*</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/plots/FTEbyActivity.php">FTEbyActivity</a>
            <a class="dropdown-item" href="<?= $report_server ?>/plots/FTEbySystem.php">FTEbySystem</a>
          </div>
        </li>
      -->
    </ul>

    <ul class="navbar-nav">
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Checks
          </a>
          <div class="dropdown-menu">
          <!-- placement exactly under menu
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
          -->
            <a class="dropdown-item" href="<?= $report_server ?>/checks/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/checks/CategoryUsage.php">Category Usage</a>
            <a class="dropdown-item" href="<?= $report_server ?>/checks/ConflictingShifts.php">Conflicting Shifts</a>
            <a class="dropdown-item" href="<?= $report_server ?>/checks/DoubleDesks.php">Double Desks</a>
            <a class="dropdown-item" href="<?= $report_server ?>/checks/OvervaluedShifts.php">Overvalued Shifts</a>
            <a class="dropdown-item" href="<?= $report_server ?>/checks/OnShift.php">On Shift</a>
          </div>
        </li>
      <!--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Tests
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="<?= $report_server ?>/tests/GetClientAddress.php">Get Client Address</a>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/PublicPhoneListRedirect.html">Public Phonelist Redirect</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/BootstrapSelectTest.html">Bootstrap Select Test</a>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/OtpBootstrapSelectTest.php">OTP Bootstrap Select Test</a>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/ScrollTest.html">Scroll Test</a>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/OtpScrollTest.php">OTP Scroll Test</a>

            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header btn-danger">Not Working</h6>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/TrainingIndico.php">Training Indico</a>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/UnicodeTest.php">Unicode Test</a>
          </div>
        </li>
      -->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Admin
          </a>
          <div class="dropdown-menu">
            <div class="dropdown-divider">Info</div>
            <a class="dropdown-item" href="https://twiki.cern.ch/twiki/bin/view/Atlas/OTP">OTP Twiki...(redirect)</a>
            <div class="dropdown-divider">Development</div>
            <a class="dropdown-item" href="https://gitlab.cern.ch/atlas-otp/otp-www/-/milestones">OTP Report Milestones</a>
            <a class="dropdown-item" href="https://gitlab.cern.ch/atlas-otp">OTP Gitlab Repositories</a>
            <a class="dropdown-item" href="https://groups-portal.web.cern.ch">E-groups atlas-otp (grappa)</a>
            <a class="dropdown-item" href="https://e-groups.cern.ch/e-groups/EgroupsSearchForm.do">E-groups atlas-otp (old)</a>
            <a class="dropdown-item" href="https://cern.service-now.com">Service Now</a>
            <div class="dropdown-divider">Apps</div>
            <a class="dropdown-item" href="https://atlas-otp.cern.ch/">Prod OTP Tool</a>
            <a class="dropdown-item" href="https://preprodatlas-otp.cern.ch/">Beta OTP Tool</a>
            <a class="dropdown-item" href="https://testatlas-otp.cern.ch/">Test OTP Tool</a>
            <a class="dropdown-item" href="https://devatlas-otp.cern.ch/">Dev OTP Tool</a>
            <!--
            <a class="dropdown-item" href="https://atlasop.cern.ch/otp/PhoneList.php">Public Phonebook Page</a>
            <div class="dropdown-divider">Archive</div>
            <a class="dropdown-item" href="https://espace.cern.ch/atlas-otp/Shared%20Documents/reporting.aspx?PageView=Shared">Sharepoint Reports</a>
            -->
          </div>
        </li>
      <!--
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li>
      -->
      </ul>
    <!--
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    -->
    </div>
  </nav>

  <h3 id="database-tables">Database Tables</h3>

<p>Several public read-only tables are defined, with similar names as their originals,
below in alphabetical order. Some tables are Summaries and are calculated every 30 minutes:</p>

<h4 id="pub_commitments-commitments-for-institutes">PUB_COMMITMENTS (Commitments for Institutes)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ID</td>
      <td style="text-align: left">Commitment ID</td>
    </tr>
    <tr>
      <td style="text-align: left">YEAR</td>
      <td style="text-align: left">Year for the commitment</td>
    </tr>
    <tr>
      <td style="text-align: left">VALUE</td>
      <td style="text-align: left">Value for the commitment</td>
    </tr>
    <tr>
      <td style="text-align: left">RES_REQUIREMENT_ID</td>
      <td style="text-align: left">PUB_RES_REQUIREMENT ID or PUB_TASK ID (exclusive) if commitment is on task</td>
    </tr>
    <tr>
      <td style="text-align: left">INSTITUTE_ID</td>
      <td style="text-align: left">PUB_INSTITUTE ID</td>
    </tr>
    <tr>
      <td style="text-align: left">DESCRIPTION</td>
      <td style="text-align: left">Description</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_domain_lists-list-of-options-for-some-fields">PUB_DOMAIN_LISTS (List of Options for some Fields)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">CODE</td>
      <td style="text-align: left">Code for Label , 1, 2, Class 1, …</td>
    </tr>
    <tr>
      <td style="text-align: left">LABEL</td>
      <td style="text-align: left">Category 1, Category 2, Class 1, …</td>
    </tr>
    <tr>
      <td style="text-align: left">DESCRIPTION</td>
      <td style="text-align: left">Description of the label</td>
    </tr>
    <tr>
      <td style="text-align: left">ORDER_IN_TYPE</td>
      <td style="text-align: left">Order in which is appears in select button</td>
    </tr>
    <tr>
      <td style="text-align: left">ALLOW_SELECT</td>
      <td style="text-align: left">Allow selection of this option</td>
    </tr>
    <tr>
      <td style="text-align: left">TYP_CODE</td>
      <td style="text-align: left">Domain Type: Category, MaintPeriodStatus, ResourceRequirementType, ResourceRequirementRecognition, Hours, TaskLocation</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_funding_agency-funding-agency-details">PUB_FUNDING_AGENCY (Funding Agency Details)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ID</td>
      <td style="text-align: left">Funding Agency ID</td>
    </tr>
    <tr>
      <td style="text-align: left">NAME</td>
      <td style="text-align: left">Name of the Funding Agency</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_institute-institute-details">PUB_INSTITUTE (Institute Details)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ID</td>
      <td style="text-align: left">Institute ID, Institutes &lt; 1000, Clusters 1000+, Commitment Clusters 2000+, Tier-x 5000+</td>
    </tr>
    <tr>
      <td style="text-align: left">ONAME</td>
      <td style="text-align: left">Short Name of Institute, prepended by IC: for Commitment Clusters</td>
    </tr>
    <tr>
      <td style="text-align: left">NAME</td>
      <td style="text-align: left">Long Name of Institute, CSV PUB_INSTITUTE ID for Commitment Clusters</td>
    </tr>
    <tr>
      <td style="text-align: left">FUNDA_ID</td>
      <td style="text-align: left">PUB_FUNDING_AGENCY ID</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_real_institute-institute-details-without-clustering">PUB_REAL_INSTITUTE (Institute Details, without clustering)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ID</td>
      <td style="text-align: left">Institute ID, Institutes &lt; 1000, Clusters 1000+, Commitment Clusters 2000+, Tier-x 5000+</td>
    </tr>
    <tr>
      <td style="text-align: left">ONAME</td>
      <td style="text-align: left">Short Name of Institute, prepended by IC: for Commitment Clusters</td>
    </tr>
    <tr>
      <td style="text-align: left">NAME</td>
      <td style="text-align: left">Long Name of Institute, CSV PUB_INSTITUTE ID for Commitment Clusters</td>
    </tr>
    <tr>
      <td style="text-align: left">FUNDA_ID</td>
      <td style="text-align: left">PUB_FUNDING_AGENCY ID</td>
    </tr>
    <tr>
      <td style="text-align: left">CLUSTER_ID</td>
      <td style="text-align: left">PUB_REAL_INSTITUTE ID this institute is a member or associate of</td>
    </tr>
    <tr>
      <td style="text-align: left">IS_CLUSTER</td>
      <td style="text-align: left">Flag indicates this is a cluster</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_institute_requirement-requirement-distribution-ot-for-institutes">PUB_INSTITUTE_REQUIREMENT (Requirement Distribution (OT) for Institutes)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">YEAR</td>
      <td style="text-align: left">Year for the OT Value (as of 2018 prepended by 3, e.g. 32018, for Class 3 OTs)</td>
    </tr>
    <tr>
      <td style="text-align: left">INSTITUTE_ID</td>
      <td style="text-align: left">PUB_INSTITUTE ID</td>
    </tr>
    <tr>
      <td style="text-align: left">REQ_VALUE</td>
      <td style="text-align: left">OT Value</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_last_institute-last-institute-for-a-person-contract">PUB_LAST_INSTITUTE (Last Institute for a person (contract))</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">PERSON_ID</td>
      <td style="text-align: left">PUB_PERSON_ID</td>
    </tr>
    <tr>
      <td style="text-align: left">INSTITUTE_ID</td>
      <td style="text-align: left">PUB_INSTITUTE ID</td>
    </tr>
    <tr>
      <td style="text-align: left">ONAME</td>
      <td style="text-align: left">Short Name of Institute, prepended by IC: for Commitment Clusters</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_monthly_pattern_aver-for-history-script-all-versions-of-the-montly-pattern">PUB_MONTHLY_PATTERN_AVER (for History script, All versions of the Montly Pattern)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ID</td>
      <td style="text-align: left">Monthly Pattern ID</td>
    </tr>
    <tr>
      <td style="text-align: left">T_WORKID</td>
      <td style="text-align: left">Transaction Work ID</td>
    </tr>
    <tr>
      <td style="text-align: left">T_START</td>
      <td style="text-align: left">Transaction Start Date in format DD-MMM-YY</td>
    </tr>
    <tr>
      <td style="text-align: left">T_DELETE</td>
      <td style="text-align: left">‘Y’ if transaction is a Delete, otherwise NULL</td>
    </tr>
    <tr>
      <td style="text-align: left">SHIFT_TYPE</td>
      <td style="text-align: left">PUB_SHIFT_PATTERN_LINE ID (Shift Type)</td>
    </tr>
    <tr>
      <td style="text-align: left">LOAD</td>
      <td style="text-align: left">Space separated load (0-100) for each day of the month</td>
    </tr>
    <tr>
      <td style="text-align: left">LOAD_CHANGED</td>
      <td style="text-align: left">‘Y’ if load was changed, otherwise ‘N’</td>
    </tr>
    <tr>
      <td style="text-align: left">MONTH_CODE</td>
      <td style="text-align: left">Code of the month in format YYYYMM</td>
    </tr>
    <tr>
      <td style="text-align: left">RES_ALLOCATION_ID</td>
      <td style="text-align: left">PUB_RES_ALLOCATION ID</td>
    </tr>
    <tr>
      <td style="text-align: left">A_CREUSER</td>
      <td style="text-align: left">PUB_PERSON ID of the user creating the Transaction</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_person-person-details">PUB_PERSON (Person Details)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ID</td>
      <td style="text-align: left">Person ID</td>
    </tr>
    <tr>
      <td style="text-align: left">AIS_PERSON_ID</td>
      <td style="text-align: left">AIS Person ID, or NULL</td>
    </tr>
    <tr>
      <td style="text-align: left">LAST_NAME</td>
      <td style="text-align: left">Last Name in uppercase</td>
    </tr>
    <tr>
      <td style="text-align: left">FIRST_NAME</td>
      <td style="text-align: left">First Name in uppercase</td>
    </tr>
    <tr>
      <td style="text-align: left">EMAIL</td>
      <td style="text-align: left">Email address, or NULL</td>
    </tr>
    <tr>
      <td style="text-align: left">PICTURE_URL</td>
      <td style="text-align: left">URL of the picture, NULL, no longer in use</td>
    </tr>
    <tr>
      <td style="text-align: left">USERNAME</td>
      <td style="text-align: left">Username at CERN, or NULL</td>
    </tr>
    <tr>
      <td style="text-align: left">PORTABLE_PHONE</td>
      <td style="text-align: left">Portable phone number, or NULL</td>
    </tr>
    <tr>
      <td style="text-align: left">LNAME</td>
      <td style="text-align: left">Last name</td>
    </tr>
    <tr>
      <td style="text-align: left">FNAME</td>
      <td style="text-align: left">First Name</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_personnel-active-personnel-for-a-particular-task-and-requirement">PUB_PERSONNEL (Active Personnel for a particular Task and Requirement)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">TASK_ID</td>
      <td style="text-align: left">PUB_TASK_ID</td>
    </tr>
    <tr>
      <td style="text-align: left">PERSON_ID</td>
      <td style="text-align: left">PUB_PERSON ID</td>
    </tr>
    <tr>
      <td style="text-align: left">RES_REQUIREMENT_ID</td>
      <td style="text-align: left">PUB_RES_REQUIREMENT ID</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_res_allocation-allocation-details">PUB_RES_ALLOCATION (Allocation Details)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ID</td>
      <td style="text-align: left">Allocation ID</td>
    </tr>
    <tr>
      <td style="text-align: left">PERSON_ID</td>
      <td style="text-align: left">PUB_PERSON ID</td>
    </tr>
    <tr>
      <td style="text-align: left">RES_REQUIREMENT_ID</td>
      <td style="text-align: left">PUB_RES_REQUIREMENT ID</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_res_requirement-requirement-details">PUB_RES_REQUIREMENT (Requirement Details)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ID</td>
      <td style="text-align: left">Requirement ID</td>
    </tr>
    <tr>
      <td style="text-align: left">TASK_ID</td>
      <td style="text-align: left">PUB_TASK ID</td>
    </tr>
    <tr>
      <td style="text-align: left">TITLE</td>
      <td style="text-align: left">Name of the Requirement</td>
    </tr>
    <tr>
      <td style="text-align: left">SHIFT_PATTERN_ID</td>
      <td style="text-align: left">SHIFT_PATTERN_ID in PUB_SHIFT_PATTERN_LINE</td>
    </tr>
    <tr>
      <td style="text-align: left">ARCHIVED</td>
      <td style="text-align: left">Requirement is Archived</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_shift_pattern_line-shift-details">PUB_SHIFT_PATTERN_LINE (Shift Details)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ID</td>
      <td style="text-align: left">Shift Pattern Type</td>
    </tr>
    <tr>
      <td style="text-align: left">DESCRIPTION</td>
      <td style="text-align: left">Description of the Shift Pattern</td>
    </tr>
    <tr>
      <td style="text-align: left">SHIFT_PATTERN_ID</td>
      <td style="text-align: left">SHIFT_PATTERN ID used in PUB_RES_REQUIREMENT</td>
    </tr>
    <tr>
      <td style="text-align: left">HOUR_FROM</td>
      <td style="text-align: left">Shift starting hour</td>
    </tr>
    <tr>
      <td style="text-align: left">HOUR_TO</td>
      <td style="text-align: left">Shift ending hour</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_sum_allocation_v-allocation-details-for-a-particular-task-requirement-and-person-filtered-by-valid-contract-pointing-to-institution">PUB_SUM_ALLOCATION_V (Allocation Details for a particular Task, Requirement and Person, filtered by valid contract, pointing to Institution)</h4>

<p>This is a summary table recalculated every 30 minutes. Contracts are updated once per day.</p>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">TASK_ID</td>
      <td style="text-align: left">PUB_TASK ID</td>
    </tr>
    <tr>
      <td style="text-align: left">RES_REQUIREMENT_ID</td>
      <td style="text-align: left">PUB_RES_REQUIREMENT ID</td>
    </tr>
    <tr>
      <td style="text-align: left">ALLOCATED_PERSON_ID</td>
      <td style="text-align: left">PUB_PERSON ID</td>
    </tr>
    <tr>
      <td style="text-align: left">ALLOCATED_PERSON_INSTITUTE_ID</td>
      <td style="text-align: left">PUB_INSTITUTE ID</td>
    </tr>
    <tr>
      <td style="text-align: left">ALLOCATED_PERSON_REAL_INSTITUTE_ID</td>
      <td style="text-align: left">PUB_REAL_INSTITUTE ID</td>
    </tr>
    <tr>
      <td style="text-align: left">ALLOCATED_FTE</td>
      <td style="text-align: left">Allocated FTE</td>
    </tr>
    <tr>
      <td style="text-align: left">ALLOCATED_HOURS</td>
      <td style="text-align: left">Allocated Hours</td>
    </tr>
    <tr>
      <td style="text-align: left">DT</td>
      <td style="text-align: left">Date in format DD-MMM-YY</td>
    </tr>
    <tr>
      <td style="text-align: left">DATE_ID</td>
      <td style="text-align: left">Date in format YYYYQMMDD, where Q is Quarter</td>
    </tr>
    <tr>
      <td style="text-align: left">REQ_TYPE_CODE</td>
      <td style="text-align: left">Requirement Type Code: Shifter, Expert</td>
    </tr>
    <tr>
      <td style="text-align: left">RECOGNITION_CODE</td>
      <td style="text-align: left">Recognition Code: Duty,Not Operation Task, …</td>
    </tr>
    <tr>
      <td style="text-align: left">HOUR_FROM</td>
      <td style="text-align: left">Shift start hour</td>
    </tr>
    <tr>
      <td style="text-align: left">HOUR_TO</td>
      <td style="text-align: left">Shift end hour</td>
    </tr>
    <tr>
      <td style="text-align: left">WBS_NODE_ID</td>
      <td style="text-align: left">PUB_WBS_NODE ID, at the time of allocation</td>
    </tr>
    <tr>
      <td style="text-align: left">SYSTEM_NODE_ID</td>
      <td style="text-align: left">PUB_SYSTEM_NODE ID, at the time of allocation</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_sum_requirement_v-requirement-details-for-a-particular-task">PUB_SUM_REQUIREMENT_V (Requirement Details for a particular Task)</h4>

<p>This is a summary table recalculated every 30 minutes.</p>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">TASK_ID</td>
      <td style="text-align: left">PUB_TASK ID</td>
    </tr>
    <tr>
      <td style="text-align: left">RES_REQUIREMENT_ID</td>
      <td style="text-align: left">PUB_RES_REQUIREMENT ID</td>
    </tr>
    <tr>
      <td style="text-align: left">REQUIRED_FTE</td>
      <td style="text-align: left">Required FTEs</td>
    </tr>
    <tr>
      <td style="text-align: left">REQUIRED_HOURS</td>
      <td style="text-align: left">Required Hours</td>
    </tr>
    <tr>
      <td style="text-align: left">DATE_ID</td>
      <td style="text-align: left">Date in format YYYYQMMDD, where Q is Quarter</td>
    </tr>
    <tr>
      <td style="text-align: left">REQ_TYPE_CODE</td>
      <td style="text-align: left">Requirement Type Code: Shifter, Expert</td>
    </tr>
    <tr>
      <td style="text-align: left">RECOGNITION_CODE</td>
      <td style="text-align: left">Recognition Code: Duty,Not Operation Task, …</td>
    </tr>
    <tr>
      <td style="text-align: left">HOUR_FROM</td>
      <td style="text-align: left">Shift start hour</td>
    </tr>
    <tr>
      <td style="text-align: left">HOUR_TO</td>
      <td style="text-align: left">Shift end hour</td>
    </tr>
    <tr>
      <td style="text-align: left">WBS_NODE_ID</td>
      <td style="text-align: left">PUB_WBS_NODE ID, at the time of requirement</td>
    </tr>
    <tr>
      <td style="text-align: left">SYSTEM_NODE_ID</td>
      <td style="text-align: left">PUB_SYSTEM_NODE ID, at the time of requirement</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_system_node-system-details">PUB_SYSTEM_NODE (System Details)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ID</td>
      <td style="text-align: left">System ID</td>
    </tr>
    <tr>
      <td style="text-align: left">TITLE</td>
      <td style="text-align: left">Name of the System</td>
    </tr>
    <tr>
      <td style="text-align: left">PARENT_ID</td>
      <td style="text-align: left">Always NULL</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_task-task-details">PUB_TASK (Task Details)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ID</td>
      <td style="text-align: left">Task ID</td>
    </tr>
    <tr>
      <td style="text-align: left">SHORT_TITLE</td>
      <td style="text-align: left">Name of the Task</td>
    </tr>
    <tr>
      <td style="text-align: left">CATEGORY_CODE</td>
      <td style="text-align: left">Category of the Task (Class 1, Class 2, …)</td>
    </tr>
    <tr>
      <td style="text-align: left">WBS_ID</td>
      <td style="text-align: left">PUB_WBS_NODE ID</td>
    </tr>
    <tr>
      <td style="text-align: left">SYSTEM_ID</td>
      <td style="text-align: left">PUB_SYSTEM_NODE ID</td>
    </tr>
    <tr>
      <td style="text-align: left">ARCHIVED</td>
      <td style="text-align: left">Task is Acrhived</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_total_allocation_v-allocation-details-for-a-particular-task-requirement-and-person">PUB_TOTAL_ALLOCATION_V (Allocation Details for a particular Task, Requirement and Person)</h4>

<p>This is a summary table recalculated every 30 minutes.</p>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">TASK_ID</td>
      <td style="text-align: left">PUB_TASK ID</td>
    </tr>
    <tr>
      <td style="text-align: left">RES_REQUIREMENT_ID</td>
      <td style="text-align: left">PUB_RES_REQUIREMENT ID</td>
    </tr>
    <tr>
      <td style="text-align: left">ALLOCATED_PERSON_ID</td>
      <td style="text-align: left">PUB_PERSON ID</td>
    </tr>
    <tr>
      <td style="text-align: left">ALLOCATED_PERSON_INSTITUTE_ID</td>
      <td style="text-align: left">PUB_INSTITUTE ID, if found</td>
    </tr>
    <tr>
      <td style="text-align: left">ALLOCATED_PERSON_REAL_INSTITUTE_ID</td>
      <td style="text-align: left">PUB_REAL_INSTITUTE ID, if found</td>
    </tr>
    <tr>
      <td style="text-align: left">ALLOCATED_FTE</td>
      <td style="text-align: left">Allocated FTE</td>
    </tr>
    <tr>
      <td style="text-align: left">ALLOCATED_HOURS</td>
      <td style="text-align: left">Allocated Hours</td>
    </tr>
    <tr>
      <td style="text-align: left">DT</td>
      <td style="text-align: left">Date in format DD-MMM-YY</td>
    </tr>
    <tr>
      <td style="text-align: left">DATE_ID</td>
      <td style="text-align: left">Date in format YYYYQMMDD, where Q is Quarter</td>
    </tr>
    <tr>
      <td style="text-align: left">REQ_TYPE_CODE</td>
      <td style="text-align: left">Requirement Type Code: Shifter, Expert</td>
    </tr>
    <tr>
      <td style="text-align: left">RECOGNITION_CODE</td>
      <td style="text-align: left">Recognition Code: Duty,Not Operation Task, …</td>
    </tr>
    <tr>
      <td style="text-align: left">HOUR_FROM</td>
      <td style="text-align: left">Shift start hour</td>
    </tr>
    <tr>
      <td style="text-align: left">HOUR_TO</td>
      <td style="text-align: left">Shift end hour</td>
    </tr>
    <tr>
      <td style="text-align: left">WBS_NODE_ID</td>
      <td style="text-align: left">PUB_WBS_NODE ID, at the time of allocation</td>
    </tr>
    <tr>
      <td style="text-align: left">SYSTEM_NODE_ID</td>
      <td style="text-align: left">PUB_SYSTEM_NODE ID, at the time of allocation</td>
    </tr>
  </tbody>
</table>

<h4 id="pub_wbs_node-work-breakdown-structure-details">PUB_WBS_NODE (Work Breakdown Structure Details)</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Field</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ID</td>
      <td style="text-align: left">Work Breakdown Structure ID</td>
    </tr>
    <tr>
      <td style="text-align: left">TITLE</td>
      <td style="text-align: left">Name of the WBS</td>
    </tr>
    <tr>
      <td style="text-align: left">PARENT_ID</td>
      <td style="text-align: left">PUB_WBS_NODE parent ID, NULL if this is the top-parent or Activity</td>
    </tr>
  </tbody>
</table>

<h3 id="python-pseudo-code">Python Pseudo Code</h3>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>import oracledb
oracledb.init_oracle_client()
otp_db = oracledb.connect('ATLAS_OTP_PUBLIC', credentials.password('ATLAS_OTP_PUBLIC'), 'ATLAS_OTP')
for col1, ...colN in otp_db.cursor().execute("SELECT ..."):
</code></pre></div></div>


  <div class="row footer navbar navbar-expand-lg navbar-dark bg-dark">
      <ul class="nav navbar-nav mr-auto">
          <li class="nav-item footer">
            <a class="nav-link" href="https://gitlab.cern.ch/atlas-otp/otp-www-next/pipelines">
              Version: 5.2.4-1-g55ece99

            </a>
          <li>
          <li class="nav-item footer"><a class="nav-link" href="<?= $web_server ?>"><?= $web_server_text ?></a><li>
      </ul>

      <ul class="nav navbar-nav">
          <li class="nav-item footer">Copyright© 2010-2025 CERN</li>
          <li class="nav-item"><a class="nav-link mail" href="mailto:Mark.Donszelmann@cern.ch?subject=ATLAS OTP Website"><i class="fa fa-envelope"></i>&nbsp;ATLAS OTP</a></li>
      </ul>
  </div>
</div>


        <!-- footer.html -->


        
        
    </body>
</html>
