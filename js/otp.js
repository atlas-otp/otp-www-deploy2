
var otpHashSelects = [];
var otpDataSelects = [];
var otpHashInputs = [];
var otpDataInputs = [];
var otpHashChecks = [];
var otpDataChecks = [];
var otpFormName;
var otpPrefixTitle;
var otpFactor;
var otpDetailPrecision;
var otpSumPrecision;
var otpPercentagePrecision;
var otpUnit;
var otpUpdateCallback;
var otpStart;
var otpEnd;

var table;

function otpStoreKey() {
  "use strict";
  return window.location.pathname + window.location.search;
}

function otpClear() {
  "use strict";
  if (typeof(Storage) !== "undefined") {
    var key = otpStoreKey();
    localStorage.removeItem(key);
  }
}

function otpStore(name, value) {
  "use strict";
  if (typeof(Storage) !== "undefined") {
    var key = otpStoreKey();
    var catalogJSON = localStorage.getItem(key);
    var catalog = {};
    if (catalogJSON !== null) {
      catalog = JSON.parse(catalogJSON);
    }
    catalog[name] = value;
    localStorage.setItem(key, JSON.stringify(catalog));
  }
}

function otpStoreSelect(name) {
  "use strict";
  var value = $('#'+name).val();
  value = value instanceof Array ? value.length === $('#'+name+' option').size() ? 'All' : value : value;
  otpStore(name, value);
}

function otpStoreCheck(name) {
  "use strict";
  var value = $('#'+name).val();
  // console.log("Store " + value);
  otpStore(name, value);
}

function otpStoreDateRange() {
  otpStore('start', otpStart);
  otpStore('end', otpEnd);
}

function otpLoad(name, defaultValue) {
  "use strict";
  if (defaultValue === undefined) defaultValue = '';
  var value = defaultValue;
  if (typeof(Storage) !== "undefined") {
    var catalogJSON = localStorage.getItem(otpStoreKey());
    if (catalogJSON !== null) {
      var newValue = JSON.parse(catalogJSON)[name];
      if ((newValue !== undefined) && (newValue !== null)) {
        value = newValue;
      }
    }
  }
  return value;
}

function otpSetDateRange() {
  console.log('yyy');
  var name = 'start';
  var start = otpStart === undefined ? otpLoad(name, otpGetDefault(name)) : otpStart;
  name = 'end';
  var end = otpEnd === undefined ? otpLoad(name, otpGetDefault(name)) : otpEnd;

  var selector = $('input[name="daterange"]');
  selector.val(start + " - " + end);
}

function otpSetSelect(name, vars, defaultValue) {
  "use strict";
  var value = vars[name] === undefined ? otpLoad(name, defaultValue) : vars[name];
  // console.log("SetSelect "+name+" = "+value+" - vars: "+vars[name]+" - default: "+defaultValue+" - load: "+otpLoad(name, "None"));
  var selector = $('#'+name);
  if (value === 'All') { // }&& (selector.prop('multiple'))) {
    // NOTE: Make sure the 'update' button goes off, keep correct order
    selector.selectpicker('selectAll');
    selector.selectpicker({
      'selectedTextFormat': 'count > -1'
    });
    selector.selectpicker('refresh');
  } else if (value instanceof Array) {
    selector.val(value);
  } else {
    // split by comma (separator) except if followed by whitespace (e.g. Yerevan,NRC KI, Protvino,NIKHEF => Yerevan; NRC KI, Protvino; NIKHEF), issue #118
    selector.val(value.split(/,(?!\s)/gm).map(function(item) {
      return item.trim();
    }));
    selector.selectpicker('refresh');
  }
}

function otpSetInput(name, vars, defaultValue) {
  "use strict";
  var value = vars[name] === undefined ? otpLoad(name, defaultValue) : vars[name];
  var selector = $('#'+name);
  selector.val(value);
}

function otpSetCheck(name, vars, defaultValue) {
  var value = vars[name] === undefined ? otpLoad(name, defaultValue) : vars[name];
  var selector = $('#'+name);
  // console.log("Set Check " + name + " " + value);
  selector.val(value);
  if (value) {
    selector.attr('checked', "checked");
  } else {
    selector.removeAttr('checked');
  }
}

function otpChangeHash() {
  "use strict";
  // var oldHash = decodeURIComponent(window.location.hash);
  var oldHash = window.location.hash;
  var newHash = otpGetHash();
  console.log('otpChangeHash.old: ' + oldHash);
  console.log('otpChangeHash.new: ' + newHash);
  if (newHash != oldHash) {
    // console.log('changed');
    if(history.replaceState) {
        history.replaceState(null, null, newHash);
        otpHashChanged(newHash);
    } else {
        window.location.hash = newHash;
    }
    return true;
  }
  return false;
}

function otpHashChanged(newHash) {
  "use strict";
  // console.log("QC: "+newHash);

  // decode hash
  var vars = [];
  var hash = newHash.split('#')[1];
  if (hash != undefined) {
    var keyValueParts = hash.split('&');
    for(var i = 0; i < keyValueParts.length; i++){
      var keyValue = keyValueParts[i].split('=');
      switch(keyValue[0]) {
        case 'start':
          otpStart = decodeURIComponent(keyValue[1]);
          break;
        case 'end':
          otpEnd = decodeURIComponent(keyValue[1]);
          break;
        default:
          // vars.push(keyValue[1]);
          vars[decodeURIComponent(keyValue[0])] = decodeURIComponent(keyValue[1]);
          break;
      }
    }
  }

  for (var j = 0; j < otpHashSelects.length; j++) {
    otpSetSelect(otpHashSelects[j], vars, otpGetDefault(otpHashSelects[j]));
  }
  for (var k = 0; k < otpDataSelects.length; k++) {
    otpSetSelect(otpDataSelects[k], vars, otpGetDefault(otpDataSelects[k]));
  }
  for (var l = 0; l < otpHashInputs.length; l++) {
    otpSetInput(otpHashInputs[l], vars, otpGetDefault(otpHashInputs[l]));
  }
  for (var m = 0; m < otpDataInputs.length; m++) {
    otpSetInput(otpDataInputs[m], vars, otpGetDefault(otpDataInputs[m]));
  }
  for (var n = 0; n < otpHashChecks.length; n++) {
    otpSetCheck(otpHashChecks[n], vars, otpGetDefault(otpHashChecks[n]));
  }
  for (var o = 0; o < otpDataChecks.length; o++) {
    otpSetCheck(otpDataChecks[o], vars, otpGetDefault(otpDataChecks[o]));
  }

  otpSetDateRange();

  // recursive until no change
  if (!otpChangeHash()) {
//    console.log("no more changes");
    otpChangeData();
  }
}

// Returns "A, B, C and D" for ["A", "B", "C", "D"], "" for null
function otpNiceValue(value) {
  "use strict";
  if (value === null) {
    return "";
  }
  if (value instanceof Array) {
    return [ value.slice(0, value.length - 1 || 1).join(", ")].concat(value.slice().splice(-1, Number(value.length > 1))).join(" and ");
  }
  return value;
}

function otpGetValue(name) {
  "use strict";
  return otpNiceValue($('#'+name).val());
}

function otpGetValueOrAll(name) {
  var selector = $('#'+name);
  var value = selector.val();
  if (value === null) {
    value = "";
  } else if (value instanceof Array) {
    if (value.length === $('#'+name+' option').size()) {
      value = 'All';
    }
  }
  return value;
}

function otpUpdateClass(name, value) {
  "use strict";
  if ((value === undefined) || (value === null)) {
    value = otpGetValue(name);
  }
  $('.'+name).html(value);
}

function otpUpdateDateRange() {
  $('.dates').html(otpStart + " - " + otpEnd);
}

function otpChangeData() {
  "use strict";
  for (var i = 0; i < otpHashSelects.length; i++) {
    otpStoreSelect(otpHashSelects[i]);
  }
  for (var j = 0; j < otpDataSelects.length; j++) {
    otpStoreSelect(otpDataSelects[j]);
  }
  for (var k = 0; k < otpHashInputs.length; k++) {
    otpStoreSelect(otpHashInputs[k]);
  }
  for (var l = 0; l < otpDataInputs.length; l++) {
    otpStoreSelect(otpDataInputs[l]);
  }
  for (var m = 0; m < otpHashChecks.length; m++) {
    otpStoreCheck(otpHashChecks[m]);
  }
  for (var n = 0; n < otpDataChecks.length; n++) {
    otpStoreCheck(otpDataChecks[n]);
  }
  otpStoreDateRange();

  // handle Auto Units
  var unit = $('#unit').val();
  if (unit === 'Auto Units') {
    var categories = otpGetValue('category');
    otpUnit = categories != undefined && (categories.includes('Class 1') || categories.includes('Class 2')) ? 'Shifts' : 'FTEs';
  } else {
    otpUnit = unit;
  }

  for (var o = 0; o < otpHashSelects.length; o++) {
    if (otpHashSelects[o] === 'unit') {
      otpUpdateClass('unit', otpUnit);
    } else {
      otpUpdateClass(otpHashSelects[o]);
    }
  }
  for (var p = 0; p < otpDataSelects.length; p++) {
    otpUpdateClass(otpDataSelects[p]);
  }
  for (var q = 0; q < otpHashInputs.length; q++) {
    otpUpdateClass(otpHashInputs[q]);
  }
  for (var r = 0; r < otpDataInputs.length; r++) {
    otpUpdateClass(otpDataInputs[r]);
  }
  for (var s = 0; s < otpHashChecks.length; s++) {
    otpUpdateClass(otpHashChecks[s]);
  }
  for (var t = 0; t < otpDataChecks.length; t++) {
    otpUpdateClass(otpDataChecks[t]);
  }
  otpUpdateDateRange();

  var selectionName = otpFormName + ' Report Selection';
  var reportName =  otpFormName + ' Report - ' + otpGetValue('year') + ' - ' + otpGetValue('category');
  otpUpdateClass('selectionName', selectionName);
  otpUpdateClass('reportName', reportName);
  otpUpdateClass('last_year', otpGetValue('year') - 1);
  $('title').html(otpPrefixTitle+' - '+reportName);

  // handle Units
  if (otpUnit == 'Shifts') {
    otpFactor = 365;
    otpDetailPrecision = 2;
    otpSumPrecision = 2;
    otpPercentagePrecision = 0;
  } else {
    otpFactor = 1.0;
    otpDetailPrecision = 2;
    otpSumPrecision = 2;
    otpPercentagePrecision = 0;
  }

  // Update table if defined
  if ((typeof table !== 'undefined') && (table.ajax !== undefined)) {
    var oldUrl = table.ajax.url();
    var newUrl = otpGetUrl();
    // console.log('otpChangeData: ' + oldUrl + " " + newUrl);
    if (newUrl != oldUrl) {
//      console.log("Changed");
      // initiate a new data transfer
      table.ajax.url( newUrl ).load();
//        console.log("Data "+table.ajax.url());
    } else {
      table.cells().invalidate().draw();
    }
  }

  if ((typeof otpUpdateCallback !== 'undefined') && (typeof otpUpdateCallback === "function")) {
    otpUpdateCallback();
  }
}

function otpInitReset() {
  "use strict";
  $('#Reset').on('click', function(event) {
    console.log("xxx");
    otpClear();
    otpStart = undefined;
    otpEnd = undefined;
    otpHashChanged("");
  });
}

function otpInitSelect(name, label, hashOnly) {
  "use strict";
  if (hashOnly) {
    otpHashSelects.push(name);
  } else {
    otpDataSelects.push(name);
  }

  $('#'+name).selectpicker({
    countSelectedText: function (numSelected, numTotal) {
      if (numSelected === 0) return '<span class="badge badge-warning">None</span>'; // return '<span class="badge badge-warning">No '+label+'</span>';

      if (numSelected < 4) return otpGetValue(name); // $('#'+name).val().join(', ');

      if (numSelected == numTotal) return "All"; // return "All "+label;

      return numSelected+" of "+numTotal; // return numSelected+" of "+numTotal+" "+label;
    }
  });

  $('#'+name).on('hidden.bs.select', function (e) {
    $('#'+name).selectpicker({
      'selectedTextFormat': 'count > -1'
    });
    $('#'+name).selectpicker('refresh');
    otpChangeHash();
  });


  $('#'+name).on('changed.bs.select', function (event, clickedIndex, newValue, oldValue) {
    // NOTE: happens when defaults are taken from local store
//      if (clickedIndex === undefined) return;
// if (name === 'activity') {
//  console.log("Changed xxx " + clickedIndex + " " + newValue + " " + oldValue + " " + event + " " + name);
//  $('#system').selectpicker('val', 'All');
//}

    $('#'+name).selectpicker({
      'selectedTextFormat': 'static'
    });
    $('#'+name).selectpicker('refresh');
  });
}

function otpInitSelectYear() {
  otpInitSelect('year', 'Years');
}

function otpInitDateRange() {
  "use strict";
  otpStart = otpGetDefault('start');
  otpEnd = otpGetDefault('end');
  $(function() {
    // 'input[name="daterange"]'
    $('input#dates.form-control').daterangepicker({
      locale: {
          format: 'DD-MMM-YYYY',
          separator: " - ",
          applyLabel: "Apply",
          cancelLabel: "Cancel",
          fromLabel: "From",
          toLabel: "To",
          customRangeLabel: "Custom",
          weekLabel: "W",
          daysOfWeek: [
              "Su",
              "Mo",
              "Tu",
              "We",
              "Th",
              "Fr",
              "Sa"
          ],
          monthNames: [
              "January",
              "February",
              "March",
              "April",
              "May",
              "June",
              "July",
              "August",
              "September",
              "October",
              "November",
              "December"
          ],
          firstDay: 1
      },
      opens: 'right',
      showDropdowns: true,
      showISOWeekNumbers: true,
      alwaysShowCalendars: true,
      linkedCalendars: false,
      showCustomRangeLabel: false,
      minDate: '01-Jan-2010',
      maxDate: '31-Dec-2024',
      ranges: {
        'Next 4 Weeks': [moment(), moment().add(4, 'weeks')],
        'Last 4 Weeks': [moment().subtract(4, 'weeks'), moment()],
        'Until end of the year': [moment(), moment().endOf('year')],
        ' 2024': [moment('2024-01-01'), moment('2024-12-31')],
        ' 2023': [moment('2023-01-01'), moment('2023-12-31')],
        ' 2022': [moment('2022-01-01'), moment('2022-12-31')],
        ' 2021': [moment('2021-01-01'), moment('2021-12-31')],
        ' 2020': [moment('2020-01-01'), moment('2020-12-31')],
        ' 2019': [moment('2019-01-01'), moment('2019-12-31')],
        ' 2018': [moment('2018-01-01'), moment('2018-12-31')],
        ' 2017': [moment('2017-01-01'), moment('2017-12-31')],
        ' 2016': [moment('2016-01-01'), moment('2016-12-31')],
        ' 2015': [moment('2015-01-01'), moment('2015-12-31')],
        ' 2014': [moment('2014-01-01'), moment('2014-12-31')],
        ' 2013': [moment('2013-01-01'), moment('2013-12-31')],
        ' 2012': [moment('2012-01-01'), moment('2012-12-31')],
        ' 2011': [moment('2011-01-01'), moment('2011-12-31')],
        ' 2010': [moment('2010-01-01'), moment('2010-12-31')],
      },
      startDate: otpStart,
      endDate: otpEnd,
    }, function(start, end, label) {
      // console.log("A new date selection was made: " + start.format('DD-MMM-YYYY') + ' to ' + end.format('DD-MMM-YYYY'));
      otpStart = start.format('DD-MMM-YYYY');
      otpEnd = end.format('DD-MMM-YYYY');
      otpChangeHash();
    });
  });
}

function otpInputChanged(name) {
  "use strict";
//  console.log(name+" changed "+$( "#"+name ).val());
  otpChangeHash();
}

function otpInitInput(name, hashOnly) {
  "use strict";
  if (hashOnly) {
    otpHashInputs.push(name);
  } else {
    otpDataInputs.push(name);
  }

  // handle return
  $( "#"+name ).keyup(function(event){
    if(event.keyCode == 13) {
      otpInputChanged(name);
    }
  });
  // handle blur
  $( "#"+name ).blur(function(event){
    otpInputChanged(name);
  });

}

function otpInitCheck(name, hashOnly) {
  "use strict";
  if (hashOnly) {
    otpHashChecks.push(name);
  } else {
    otpDataChecks.push(name);
  }

  $( "#"+name ).click(function() {
    var checked = $( "#"+name ).is(":checked");
    // console.log("Changed " + checked);
    $( "#"+name ).val(checked);
    otpInputChanged(name);
  });
}

function otpInitBegin(name) {
  "use strict";
  otpFormName = name;
  otpPrefixTitle = $('title').html();
  otpFactor = 1.0;
  otpDetailPrecision = 2;
  otpSumPrecision = 2;
  otpPercentagePrecision = 0;

  // get informed when hash changes
  window.addEventListener("hashchange",function(event) {
    otpHashChanged(window.location.hash);
  });

  // get informed when selection changes
  $('.selectpicker').selectpicker({
    title: '<span class="badge badge-info">Click to Update</span>',
    deselectAllText: 'None',
    selectAllText: 'All',
    selectedTextFormat: 'count > -1',
    actionsBox: true,
    width: false
  });
  $('.selectpicker').on('hidden.bs.select', function (e) {
    // hide
  });
  $('.selectpicker').on('changed.bs.select', function (event, clickedIndex, newValue, oldValue) {
    // any change
    // console.log("Changed selection " + clickedIndex + " " + newValue + " " + oldValue + " " + event + " " + name);
  });
}

function otpInitEnd(updateCallback) {
  "use strict";

  if ((typeof updateCallback !== 'undefined') && (typeof updateCallback === "function")) {
    otpUpdateCallback = updateCallback;
  }

  // initial setup of hash, from real url, localstore and defaults
  otpHashChanged(window.location.hash);
}

function otpGetQuery(info) {
  "use strict";
  var query = "";
  for (var i = 0; i < info.length; i++) {
    var name = info[i];
    var value = otpGetValueOrAll(name);
    query += '&' + encodeURIComponent(name) + '=' + encodeURIComponent(value);
  }
  return query;
}

function otpGetDateRange() {
  "use strict";
  var query = "";
  if (otpStart !== undefined) {
    query += '&start=' + encodeURIComponent(otpStart);
  }
  if (otpEnd !== undefined) {
    query += '&end=' + encodeURIComponent(otpEnd);
  }
  return query;
}

function otpGetHash() {
  "use strict";
  var hash = otpGetQuery(otpDataSelects) +
             otpGetQuery(otpHashSelects) +
             otpGetQuery(otpDataInputs) +
             otpGetQuery(otpHashInputs) +
             otpGetQuery(otpDataChecks) +
             otpGetQuery(otpHashChecks) +
             otpGetDateRange() +
             window.location.search.replace('?', '&');
  return hash.replace('&', '#');
}

function otpGetUrl() {
  "use strict";
  var search = window.location.search;
  var query = search.replace('?', '&') +
              otpGetQuery(otpDataSelects) +
              otpGetQuery(otpDataInputs) +
              otpGetQuery(otpDataChecks) +
              otpGetDateRange();
  // NOTE, the query part of the url needs to have the + (plus) encoded as %2B
  // the + (plus) otherwise denotes a space. All other special characters seem
  // to be handled automatically.
  query = query.replace(/\+/g, '%2B');
  var url = "./"+otpFormName+"Data.php"+query.replace('&', '?');
  return url;
}

function otpGetColorClass(p) {
  "use strict";
  var bad = 0;
  var average = 50;
  var good = 90;
  var excellent = 110;

  if ((p === 'N/A') || (p < 0) || (p === null)) {
    return 'na';
  }

  if ((typeof p === 'string') && p.endsWith('%')) {
    p = p.slice(0, -1);
  }

  p = Number(p);

  if (p < bad) {
    return 'na';
  } else if ((bad <= p) && ( p < average )) {
    return 'bad';
  } else if ((average <= p) && (p < good)) {
    return 'average';
  } else if ((good <= p) && (p < excellent)) {
    return 'good';
  } else if (excellent <= p) {
    return 'excellent';
  } else {
    return 'unknown';
  }
}

function otpGetLanguage(time) {
  var timeString = time > 0 ? ' (' + time + 's)' : '';
  return {
    "loadingRecords": '<h4><span class="badge badge-info">Please wait - Loading OTP Data' + timeString + '...</span></h4>',
    "emptyTable": '<h4><span class="badge badge-warning">No OTP Data for Selection</span></h4>',
    "zeroRecords": '<h4><span class="badge badge-warning">No OTP Data for Search</span></h4>'
  };
}

function otpGetExport() {
  return [
    'copy', 'csv',
    {
      text: 'Excel',
      extend: 'excelHtml5',
      footer: true,
      exportOptions: {
        columns: ':visible',
      }
    },
    {
      text: 'PDF',
      extend: 'pdfHtml5',
      orientation: 'landscape',
      pageSize: 'A3',
      footer: true,
      exportOptions: {
        columns: ':visible',
      },
    },
    {
      text: 'Print',
      extend: 'print',
      autoPrint: true,
      orientation: 'landscape',
      pageSize: 'A4',
      footer: true,
      exportOptions: {
        columns: ':visible',
      },
      xcustomize: function (win) {
          // FIXME: sums need coloring
          // FIXME: printing does not show color

          $(win.document.body).find('thead tr th').each(function(index) {
            $(this).css('background-color', 'dodgerblue');
            $(this).css('color', 'white');
          });

          $(win.document.body).find('tfoot tr th').each(function(index) {
            var col = index % 8;
            if (col <= 1) {
              $(this).css('background-color', 'dodgerblue');
              $(this).css('color', 'white');
            }
          });

          $(win.document.body).find('tr:nth-child(even) td').each(function(index) {
            $(this).css('background-color', 'white');
          });

          $(win.document.body).find('tr td').each(function(index) {
              var row = Math.floor(index / 8);
              var col = index % 8;

              if ((col == 0) || (col == 1)) {
                $(this).css('background-color', 'dodgerblue');
                $(this).css('color', 'white');
              }

              var cols = [ 4, 6, 7 ];
              if (cols.indexOf(col) >= 0) {
                var cls = otpGetColorClass($(this).text());
                switch(cls) {
                  case 'title':
                    $(this).css('background-color', 'dodgerblue');
                    $(this).css('color', 'white');
                    break;
                  case 'ic':
                    $(this).css('background-color', 'darkblue');
                    $(this).css('color', 'white');
                    break;
                  case 'na':
                    $(this).css('background-color', 'purple');
                    $(this).css('color', 'white');
                    break;
                  case 'bad':
                    $(this).css('background-color', 'red');
                    $(this).css('color', 'white');
                    break;
                  case 'average':
                    $(this).css('background-color', 'yellow');
                    $(this).css('color', 'black');
                    break;
                  case 'good':
                    $(this).css('background-color', 'green');
                    $(this).css('color', 'white');
                    break;
                  case 'excellent':
                    $(this).css('background-color', 'cyan');
                    $(this).css('color', 'black');
                    break;
                  case 'estimate':
                    $(this).css('font-style', 'italic');
                      break;
                  default:
                    console.error("unimplemented style class: " + cls + " in col " + col + " with text: " + $(this).text());
                }
              } else if (col == 1 && $(this).text().startsWith("IC: ")) {
                $(this).css('background-color', 'indigo');
                $(this).css('color', 'white');
              }
          });
      }
    }
  ];
}

function otpIsNumeric(n) {
  "use strict";
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function otpIsEstimate(estimate, real) {
  "use strict";
  var e = otpIsNumeric(estimate) ? estimate : Number(estimate);
  var r = otpIsNumeric(real) ? real : Number(real);
  var limit = 0.05;
  return (e - e*limit) > r;
}

function otpGetEstimate(estimate, real) {
  "use strict";
  return otpIsEstimate(estimate, real) ? estimate : real;
}

function otpPercentage(part, over) {
  var result = over === 0 || over === null ? Number(-1) : Number(part * 100 / over);
  return result;
}

var otpDoubleValue = function ( i ) {
    return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
            i : 0;
};

var otpSum = function (a, b) {
  return otpDoubleValue(a) + otpDoubleValue(b);
};
