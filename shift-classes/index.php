<!DOCTYPE html>

<!-- warning.html -->

<!--------------------------------------------------------------------------->
<!--                                                                       -->
<!-- WARNING, this file is generated, please edit the source and reconvert -->
<!--                                                                       -->
<!--                                                                       -->
<!--                                                                       -->
<!--------------------------------------------------------------------------->



<?php
error_reporting(E_ALL);
ini_set("display_errors", '1');
?>


<?php
date_default_timezone_set("Europe/Zurich");
$dateformat = "d-m-y H:i:s";
$utcformat = "D, d M Y H:i:s T";
$updatetime = 15*60;

set_include_path(get_include_path() . PATH_SEPARATOR . '../');
?>


<html lang="en">
    <head>
        <!-- header.html -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="OTP, Operation Task Planner">
<meta name="keywords" content="CERN, ATLAS, OTP">
<meta name="author" content="Mark Donszelmann">
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">

<title>
   &middot; OTP ATLAS
</title>

<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../ico/atlas-144x144.png">
                               <link rel="shortcut icon" href="../ico/atlas.ico">

<!-- jQuery -->
<!-- NOTE: no Ajax in 'slim' version of jquery -->
<!-- NOTE: jquery 3.x did not work -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- jQuery TimeAgo -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.6.3/jquery.timeago.min.js"></script>

<!-- jQuery Resizable -->
<!--
<script src="../jquery-resizable-0.28/dist/jquery-resizable.min.js"></script>
-->

<!-- Bootstrap -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.2.1/cerulean/bootstrap.min.css">
<!--
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css">
-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" >

<!-- Bootstrap Select -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/css/bootstrap-select.min.css">

<!-- Bootstrap DateRangePicker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js"></script>

<!-- Datatables, jquery then bootstrap, but NOTE removal below -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.min.css" >
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
<!-- Removing container-fluid on class of dataTables_wrapper as it interferes with scrollResize -->
var DataTable = $.fn.dataTable;
$.extend( DataTable.ext.classes, {
    sWrapper:      "dataTables_wrapper dt-bootstrap4",
} );
</script>

<!-- Datatables Extensions -->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.0.3/css/rowGroup.dataTables.min.css">
<script src="https://cdn.datatables.net/rowgroup/1.0.3/js/dataTables.rowGroup.min.js"></script>

<!-- Does not work with scrollResize
<link rel="stylesheet" href="https://cdn.datatables.net/scroller/1.5.0/css/scroller.dataTables.min.css">
<script src="https://cdn.datatables.net/scroller/1.5.0/js/dataTables.scroller.min.js"></script>
-->

<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.4/css/fixedHeader.dataTables.min.css">
<script src="https://cdn.datatables.net/fixedheader/3.1.4/js/dataTables.fixedHeader.min.js"></script>

<!-- Datatables Plugins -->
<script src="https://cdn.datatables.net/plug-ins/1.10.19/features/scrollResize/dataTables.scrollResize.min.js"></script>

<!-- Highcharts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/highcharts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/modules/exporting.js"></script>

<!-- Search JS -->
<!--
Below are possibly these ?
<script src="https://cdnjs.cloudflare.com/ajax/libs/lunr.js/2.0.1/lunr.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="http://stevenlevithan.com/assets/misc/date.format.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/URI.js/1.18.10/URI.min.js" type="text/javascript"></script>
-->
<!--
<script src="../js/search.min.js" type="text/javascript"></script>
-->
<script src="../js/lunr.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/mustache.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/date.format.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/URI.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/jquery.lunr.search.js" type="text/javascript" charset="utf-8"></script>

<!-- ATLAS and OTP Stylesheets and JavaScript-->
<link rel="stylesheet" href="../css/atlas.css" >
<link rel="stylesheet" href="../css/otp.css" >
<script src="../js/otp.js" type="text/javascript"></script>




        
        
    </head>

    <body>
        <?php
if (str_contains($_SERVER['SERVER_NAME'], '-alpha')) {
  $report_server = "https://otp-reports-atlas-alpha.web.cern.ch";
  $web_server = "https://otp-atlas.web.cern.ch";
  $web_server_text = "Goto Production Website";
} else {
  $report_server = "https://otp-reports-atlas.web.cern.ch";
  $web_server = "https://otp-www-atlas-alpha.web.cern.ch";
  $web_server_text = "Goto Staged Website";
}
?>


<div class="container">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="../">ATLAS OTP</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

        <li class="nav-item">

        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Shifts
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../docs/QuickStart.php">Quick Start</a>
            <a class="dropdown-item" href="../shift-classes/index.php">Shift Classes and Requirements</a>
            <a class="dropdown-item" href="../docs/ControlRoomShifts.php">ATLAS Control Room Shifts</a>
            <a class="dropdown-item" href="https://atlasop.cern.ch/atlas-point1/shifts">Safety Rules for Shifters</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Documentation
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../docs/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../docs/WhatIsOTP.pdf" target="WhatIsOTP">What is OTP</a>
            <a class="dropdown-item" href="https://otp-atlas.docs.cern.ch" target="OTPUserManual">OTP User Manual</a>
            <a class="dropdown-item" href="../docs/API.php">Public API</a>
            <a class="dropdown-item" href="../docs/AUTHDB.php">AUTHDB Usage</a>
            <a class="dropdown-item" href="../docs/Calculations.php">Calculations</a>
            <a class="dropdown-item" href="../docs/FAQ.php">FAQ</a>
            <a class="dropdown-item" href="../docs/OTvalues.php">OT Values</a>
            <a class="dropdown-item" href="../docs/Photos.php">Photos</a>
            <a class="dropdown-item" href="../docs/SignupForShifts.php">How to Signup for Shifts</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Tools
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="https://atlas-otp.cern.ch/" target="otp-tool">OTP Tool...</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Reports
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="<?= $report_server ?>/reports/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/TaskScheduleP1.php">Task Schedule Point1</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/FundingAgency.php">Funding Agency</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/FundingAgency.php?byInstitution">Funding Agency by Institution</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Institution.php">Institution</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/InstitutionYear.php">Institution by Year</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Task.php">Task</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/MultiYear.php">Multi-Year</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/MultiYear.php?byInstitution">Multi-Year by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Commitment.php">Commitment</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/CommitmentFunding.php">Commitment Funding</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/CommitmentFunding.php?byInstitution">Commitment Funding by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/PersonalSchedule.php">Personal Schedule</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/TaskSchedule.php">Task Schedule</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Occupancy.php">Occupancy</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Personal.php">Personal</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/ShiftsByHour.php">Shifts by Hour (SLIMOS)</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/TaskPersonnel.php">Task Personnel</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Allocation.php">Allocation</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/Requirement.php">Requirement</a>
            <a class="dropdown-item" href="<?= $report_server ?>/reports/History.php">History</a>
          </div>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?= $report_server ?>/final_reports/index.php">Final Reports</a>
        </li>

      <!--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Plots
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="<?= $report_server ?>/plots/index.php">Description*</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/plots/FTEbyActivity.php">FTEbyActivity</a>
            <a class="dropdown-item" href="<?= $report_server ?>/plots/FTEbySystem.php">FTEbySystem</a>
          </div>
        </li>
      -->
    </ul>

    <ul class="navbar-nav">
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Checks
          </a>
          <div class="dropdown-menu">
          <!-- placement exactly under menu
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
          -->
            <a class="dropdown-item" href="<?= $report_server ?>/checks/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/checks/CategoryUsage.php">Category Usage</a>
            <a class="dropdown-item" href="<?= $report_server ?>/checks/ConflictingShifts.php">Conflicting Shifts</a>
            <a class="dropdown-item" href="<?= $report_server ?>/checks/DoubleDesks.php">Double Desks</a>
            <a class="dropdown-item" href="<?= $report_server ?>/checks/OvervaluedShifts.php">Overvalued Shifts</a>
            <a class="dropdown-item" href="<?= $report_server ?>/checks/OnShift.php">On Shift</a>
          </div>
        </li>
      <!--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Tests
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="<?= $report_server ?>/tests/GetClientAddress.php">Get Client Address</a>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/PublicPhoneListRedirect.html">Public Phonelist Redirect</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/BootstrapSelectTest.html">Bootstrap Select Test</a>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/OtpBootstrapSelectTest.php">OTP Bootstrap Select Test</a>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/ScrollTest.html">Scroll Test</a>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/OtpScrollTest.php">OTP Scroll Test</a>

            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header btn-danger">Not Working</h6>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/TrainingIndico.php">Training Indico</a>
            <a class="dropdown-item" href="<?= $report_server ?>/tests/UnicodeTest.php">Unicode Test</a>
          </div>
        </li>
      -->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Admin
          </a>
          <div class="dropdown-menu">
            <div class="dropdown-divider">Info</div>
            <a class="dropdown-item" href="https://twiki.cern.ch/twiki/bin/view/Atlas/OTP">OTP Twiki...(redirect)</a>
            <div class="dropdown-divider">Development</div>
            <a class="dropdown-item" href="https://gitlab.cern.ch/atlas-otp/otp-www/-/milestones">OTP Report Milestones</a>
            <a class="dropdown-item" href="https://gitlab.cern.ch/atlas-otp">OTP Gitlab Repositories</a>
            <a class="dropdown-item" href="https://groups-portal.web.cern.ch">E-groups atlas-otp (grappa)</a>
            <a class="dropdown-item" href="https://e-groups.cern.ch/e-groups/EgroupsSearchForm.do">E-groups atlas-otp (old)</a>
            <a class="dropdown-item" href="https://cern.service-now.com">Service Now</a>
            <div class="dropdown-divider">Apps</div>
            <a class="dropdown-item" href="https://atlas-otp.cern.ch/">Prod OTP Tool</a>
            <a class="dropdown-item" href="https://preprodatlas-otp.cern.ch/">Beta OTP Tool</a>
            <a class="dropdown-item" href="https://testatlas-otp.cern.ch/">Test OTP Tool</a>
            <a class="dropdown-item" href="https://devatlas-otp.cern.ch/">Dev OTP Tool</a>
            <!--
            <a class="dropdown-item" href="https://atlasop.cern.ch/otp/PhoneList.php">Public Phonebook Page</a>
            <div class="dropdown-divider">Archive</div>
            <a class="dropdown-item" href="https://espace.cern.ch/atlas-otp/Shared%20Documents/reporting.aspx?PageView=Shared">Sharepoint Reports</a>
            -->
          </div>
        </li>
      <!--
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li>
      -->
      </ul>
    <!--
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    -->
    </div>
  </nav>

  <h3 id="otp-shift-classes">OTP Shift Classes</h3>

<p>Updated: 19-03-2024</p>

<!--
Older versions of this page are found here: OtpShiftClasses2019, OtpShiftClasses2018.
-->

<p>Based upon operational experience to date and after discussion in several venues and endorsement by the Executive Board, ATLAS Management has decided upon the following procedure for sharing of ATLAS Operation Tasks for 2010 and thereafter.</p>

<ol>
  <li>Operation Tasks (OTs) are regrouped into three categories:
    <ul>
      <li>Class 1: <strong>ACR shifts</strong> - Central and Detector Shifts in ATLAS Control Room at Point-1.</li>
      <li>Class 2: <strong>Other shifts</strong> - Additional shifts, including shifts in satellite control rooms, computing shifts, remote shifts, on-call shifts.</li>
      <li>Class 3: <strong>Expert operation tasks</strong> - Operation tasks involving experts on systems, data preparation, computing, software.</li>
    </ul>
  </li>
  <li>Operation Tasks are defined by the Project Leaders and Activity Coordinators. Class 1 and Class 2 Operation Tasks have units of “shifts”. One shift unit corresponds to an 8-hour ACR shift or an equivalent workload (perhaps distributed over more than 8 hours, for instance on-call). Class 3 Operation Tasks have units of “FTE”.</li>
  <li>Within Class 1,
    <ul>
      <li>before 2023, weekend and night shifts carry a weight factor (1.31) that is double that of day and evening shifts on weekdays (0.66).</li>
      <li>since 2023, the weight factors changed to: day and evening shifts on weekdays (0.60), double that on Monday to Thursday night shifts and weekend day and evening shifts (1.2), and triple that on Friday to Sunday night shifts (1.8).  Official CERN holidays are treated as if they were a Sunday.</li>
    </ul>
  </li>
  <li>Institutions are expected to contribute to each of the three classes according to their OT share. As of 2015 Class 1 and Class 2 are combined in OT one can therefore do either. Activities of one class cannot be freely substituted for activities of another class. For example, Class 3 OTs (expert operation tasks) cannot  be substituted for Class 1 OTs (ACR shifts) or Class 2 (On-call shifts).</li>
  <li>A call will be sent to all Institutions for contributions to Class 1 and Class 2 OTs. The call will provide the number of shifts units  expected for each Institution. This call will also include the required contributions to Class 3 OTs, providing the number of “FTE” expected from each Institution.</li>
</ol>

<p>In addition, there are Class 4 shifts (tasks related to the operation of the Tier-1 and Tier-2 computing centres) but these are primarily intended for institutions involved in Tier-1 and Tier-2 operations.</p>

<p><strong>OTP allocation upon qualification as an ATLAS author:</strong> After successful completion of a Authorship Qualification Project (formerly known as Qualification Task, QT), 0.24 FTE of OTP will be allocated to the qualifying author when their project is completed, in recognition of the contributions associated with the project. The OTP is allocated from the appropriate pool being overseen by the PL/AC. This is one-time-only allocation and not normally depend on the actual length or effort associated with the qualification project. This OTP is normally automatically allocated approximately one month after the completion of the task.</p>

<p><strong>OT Primer:</strong> Do you find OT hard to understand? <a href="../docs/WhatIsOTP.pdf">Here</a> are some slides on:</p>
<ul>
  <li>Explaining OT to non-ATLAS people (some helpful text in case you are asked to explain why your team needs to do OT)</li>
  <li>OT details with cartoons… (might be helpful in explaining OT math)</li>
  <li>Understanding OT team requirement  (a walk through on how to derive your team’s OT requirement numbers)</li>
</ul>

<h4 id="call-for-class-1-and-class-2-operation-tasks-shifts">Call for Class 1 and Class 2 Operation Tasks (shifts)</h4>

<p>The total number of Class 1 and Class 2 Operation Tasks for the period from January to December for any given year is estimated in consultation with the Project Leaders and Activity Coordinators. The number of shifts required from each Institution is calculated based on the estimated total number of shifts that must be filled and on each Institution’s Operation Task (OT) share. The Institution OT share is the Institution OT divided by the total ATLAS OT. The reference documents for OT share can be found under as <em>attachments</em> to  an Authorship Committee twiki page: <a href="https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ATLASAuthorshipMandO">https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ATLASAuthorshipMandO</a> (look for the relevant year at the bottom of the page) which lists first by Institute, then by Institution, and finally by Funding Agency).</p>

<h4 id="how-many-class-1-and-class-2-shifts-are-requested-in-any-given-year">How many Class 1 and Class 2 shifts are requested in any given year?</h4>

<p>The numbers of Class 1 shifts and Class 2 shifts corresponding to each Institution’s OT share for the period from January to December are listed for each year in the table below in the column “Required Shifts per Institution”. Each institution is requested to perform both the number of Class 1 and Class 2 shifts listed. The record of each institution in performing its requested number of shifts over time is monitored for these classes.</p>

<h4 id="how-can-a-given-institution-contribute-to-class-1-and-class-2-shifts">How can a given Institution contribute to Class 1 and Class 2 shifts?</h4>

<p>Each Institution can contribute to the Class 1 and Class 2 shifts of any sub-system/activity specified in Table 1 below. The names or e-groups of the corresponding contact persons are also provided in the table. There are two ways to contribute:</p>
<ol>
  <li>Wait for the “shift call” email from the sub-system contact persons, and answer the call by specifying how much your Institution wants to contribute and what class of contribution (Class 1 or Class 2). Note that your Institution can contribute to more than one activity of Table 1.</li>
  <li>Contact directly the person(s) listed in Table 1, without waiting for the “shift call” e-mail. This is the recommended option if your Institution intends to contribute to a sub-system/activity or general shifts in which it did not previously participate.</li>
</ol>

<p>There are several common ATLAS-wide Class 1 shift tasks that do not belong to any of the traditional projects and thus do not have a set of Institutions to rely upon to fill shifts. These tasks are the following: Shift Leader, Run Control Shifter, Data Quality and Shift Leader in Matters of Safety (SLIMOS). Therefore, every Institution is also encouraged to participate to these central activities beyond their traditional commitments.</p>

<p>Shifter training and documentation are provided for each activity, and are under the responsibility of the corresponding project leaders/ activity coordinators. Specific rules concerning shifts in ATLAS are available at: <a href="https://atlasop.cern.ch/shifts">https://atlasop.cern.ch/shifts</a>.</p>

<h4 id="reminder-concerning-class-3-expert-operation-tasks">Reminder concerning Class 3 Expert Operation Tasks</h4>

<p>Class 1 and Class 2 shifts represent only a small fraction of the all Operation Tasks necessary to operate ATLAS. Class 3 OTs are expert operation tasks that are not shifts. Each Institution must contribute to Class 3 OTs as well as to Class 1 and Class 2. For Class 3 OTs, please contact the Project Leaders and Activity Coordinators in your area of interest <a href="http://atlas.web.cern.ch/Atlas/Management/Organization.pdf">http://atlas.web.cern.ch/Atlas/Management/Organization.pdf</a>.</p>

<h4 id="how-much-class-3-expert-task-effort-is-requested-in-any-given-year">How much Class 3 expert task effort is requested in any given year?</h4>

<p>The amount of Class 3 expert tasks (in  units of “FTE”) required to be done by each Institution is calculated based on each Institution’s OT share for the period January until December and is listed for each year in the table below in the column “Required Shifts per Institution”. The calculation of required Class-3 shifts takes into account the Class-4 and the Upgrade activities of the institution. See <a href="https://cds.cern.ch/record/2305244">Accounting of Upgrade Activities in ATLAS</a> for the details of the calculation. The record of each institution in performing its requested tasks is monitored.</p>

<h4 id="contacts-alphabetical">Contacts (alphabetical)</h4>

<p>For an up to date list of System Run Coordinators see <a href="https://atlasop.cern.ch/otp/RunCoordinatorList.php">https://atlasop.cern.ch/otp/RunCoordinatorList.php</a></p>

<p>Table 1: Contact Persons for tasks.</p>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left"><strong>ATLAS sub-system/ activity</strong></th>
      <th style="text-align: left"><strong>Contact Persons for Class 1 &amp; Class 2 shifts</strong></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">ATLAS Run Coordination</td>
      <td style="text-align: left"><a href="mailto:atlas-run-coordinators@cern.ch">atlas-run-coordinators@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">Beam Condition Monitor</td>
      <td style="text-align: left">Marko Mikuz, Andrej Gorisek, Roshan Joshi</td>
    </tr>
    <tr>
      <td style="text-align: left">CALO+Fwd Shifter</td>
      <td style="text-align: left"><a href="mailto:atlas-calofwd-shift-organisation@cern.ch">atlas-calofwd-shift-organisation@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">Computing</td>
      <td style="text-align: left"><a href="mailto:atlas-computing-coordinators@cern.ch">atlas-computing-coordinators@cern.ch</a>, <a href="https://twiki.cern.ch/twiki/bin/view/AtlasComputing/ADCShifts">ADC shifts</a></td>
    </tr>
    <tr>
      <td style="text-align: left">Data Quality</td>
      <td style="text-align: left"><a href="mailto:atlas-dataquality-conveners@cern.ch">atlas-dataquality-conveners@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">DAQ</td>
      <td style="text-align: left">Wainer Vandelli, Adrian Chitan</td>
    </tr>
    <tr>
      <td style="text-align: left">Detector Control System</td>
      <td style="text-align: left">Stefan Schlenker, Paris Moschovakos</td>
    </tr>
    <tr>
      <td style="text-align: left">Inner Detector</td>
      <td style="text-align: left"><a href="mailto:atlas-id-runcoordinators@cern.ch">atlas-id-runcoordinators@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">LArg Calorimeter</td>
      <td style="text-align: left"><a href="mailto:atlas-lar-sys-project-leaders@cern.ch">atlas-lar-sys-project-leaders@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">Forward</td>
      <td style="text-align: left"><a href="mailto:atlas-fdet-sys-project-leaders@cern.ch">atlas-fdet-sys-project-leaders@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">Luminosity</td>
      <td style="text-align: left"><a href="mailto:atlas-luminosity-conveners@cern.ch">atlas-luminosity-conveners@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">L1 Trigger</td>
      <td style="text-align: left"><a href="mailto:atlas-tdaq-lvl1-coord@cern.ch">atlas-tdaq-lvl1-coord@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">Muon</td>
      <td style="text-align: left"><a href="mailto:atlas-muon-run-coordinators@cern.ch">atlas-muon-run-coordinators@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">Pixel</td>
      <td style="text-align: left"><a href="mailto:atlas-pixel-sys-project-leaders@cern.ch">atlas-pixel-sys-project-leaders@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">SCT</td>
      <td style="text-align: left"><a href="mailto:atlas-sct-sys-project-leaders@cern.ch">atlas-sct-sys-project-leaders@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">Shift Leader</td>
      <td style="text-align: left"><a href="mailto:atlas-run-coordinators@cern.ch">atlas-run-coordinators@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">SLIMOS/Safety</td>
      <td style="text-align: left"><a href="mailto:atlas-safety@cern.ch">atlas-safety@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">Software</td>
      <td style="text-align: left"><a href="mailto:atlas-software-coordinators@cern.ch">atlas-software-coordinators@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">Tile Calorimeter</td>
      <td style="text-align: left"><a href="mailto:atlas-tile-rc@cern.ch">atlas-tile-rc@cern.ch</a></td>
    </tr>
    <tr>
      <td style="text-align: left">Trigger</td>
      <td style="text-align: left"><a href="mailto:atlas-trigger-operation-coordinators@cern.ch">atlas-trigger-operation-coordinators@cern.ch</a>, <a href="https://twiki.cern.ch/twiki/bin/viewauth/Atlas/TriggerServiceTasks">Tasks twiki</a></td>
    </tr>
    <tr>
      <td style="text-align: left">TRT</td>
      <td style="text-align: left"><a href="mailto:atlas-trt-sys-project-leaders@cern.ch">atlas-trt-sys-project-leaders@cern.ch</a></td>
    </tr>
  </tbody>
</table>

<h4 id="class-1">Class 1</h4>

<ul>
  <li>Please see <a href="../docs/ControlRoomShifts.php">ATLAS Control Room Shifts</a>, and have a look at <a href="https://atlasop.cern.ch/twiki/bin/view/Main/ShiftTraining">ACR Shifts for Run 3</a>.</li>
</ul>

<h4 id="class-2">Class 2</h4>

<ul>
  <li>Please see <a href="https://atlas-otp.cern.ch/mao/client/cern.ppt.mao.app.gwt.MaoClient/MaoClient.html#taskList">OTP</a> and search for “Class 2”</li>
</ul>

<h4 id="class-3">Class 3</h4>

<ul>
  <li>Please see <a href="https://atlas-otp.cern.ch/mao/client/cern.ppt.mao.app.gwt.MaoClient/MaoClient.html#taskList">OTP</a> and search for “Class 3”</li>
</ul>

<h4 id="institutional-commitments">Institutional Commitments</h4>

<p>Teams are encouraged to make Institutional Commitments (IC) to Class-3 OTP tasks. An IC is the promise of an Institution to always provide a minimum FTE of human resources for specific tasks in projects/activities thereby providing more stability and continuity of human resources for these tasks and building local expertise in the home institutes. ICs can only be made for tasks where a Class-3 OTP task number exists. Here are the steps to take for a team to commit to ICs:</p>

<ul>
  <li>The Team Leader, Institution Representative, and the Project Leader/Activity Coordinator associated with the task should all agree to the necessity for the IC, the scope of the task that will be accomplished, and to the FTE level of the commitment.</li>
  <li>The Team Leader can initiate the request to create an IC by contacting <a href="mailto:vincter@physics.carleton.ca">Manuella Vincter</a> (putting the Institution Representative and Project Leader/Activity Coordinator in CC) and providing the following information
    <ul>
      <li>Name of Institution, OTP Task ID and subtask ID, Task name and subtask name, starting date of the IC (it can be backdated as far back as the first day of the current year), and the FTE level of commitment. A few additional comments can also be added for the record (for e.g. detailing the specific scope of the task).</li>
    </ul>
  </li>
</ul>

<h4 id="list-of-institutions-with-ot-share-and-number-of-required-class-1-and-class-2-shifts-and-class-3-ftes">List of Institutions with OT share and number of required Class 1 and Class 2 shifts and Class 3 FTEs</h4>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Year</th>
      <th style="text-align: left">Shifts per Institution</th>
      <th style="text-align: left">Shifts per Funding Agency</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">2024</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2024.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_funding_agency_2024.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2023</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2023.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_funding_agency_2023.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2022</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2022.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_funding_agency_2022.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2021</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2021.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_funding_agency_2021.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2020</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2020.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_funding_agency_2020.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2019</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2019.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_funding_agency_2019.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2018</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2018.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_funding_agency_2018.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2017</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2017.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_funding_agency_2017.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2016</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2016.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_funding_agency_2016.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2015</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2015.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_funding_agency_2015.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2014</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2014.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_country_2014.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2013</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2013.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_country_2013.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2012</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2012.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_country_2012.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2011</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2011.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_country_2011.pdf">PDF</a></td>
    </tr>
    <tr>
      <td style="text-align: left">2010</td>
      <td style="text-align: left"><a href="Shifts_per_institution_2010.pdf">PDF</a></td>
      <td style="text-align: left"><a href="Shifts_per_country_2010.pdf">PDF</a></td>
    </tr>
  </tbody>
</table>


  <div class="row footer navbar navbar-expand-lg navbar-dark bg-dark">
      <ul class="nav navbar-nav mr-auto">
          <li class="nav-item footer">
            <a class="nav-link" href="https://gitlab.cern.ch/atlas-otp/otp-www-next/pipelines">
              Version: 5.2.4-1-g55ece99

            </a>
          <li>
          <li class="nav-item footer"><a class="nav-link" href="<?= $web_server ?>"><?= $web_server_text ?></a><li>
      </ul>

      <ul class="nav navbar-nav">
          <li class="nav-item footer">Copyright© 2010-2025 CERN</li>
          <li class="nav-item"><a class="nav-link mail" href="mailto:Mark.Donszelmann@cern.ch?subject=ATLAS OTP Website"><i class="fa fa-envelope"></i>&nbsp;ATLAS OTP</a></li>
      </ul>
  </div>
</div>


        <!-- footer.html -->


        
        
    </body>
</html>
